# Overview #

MakeNote is a program with the goal of enhancing printed class notes. It converts single-page-per-sheet PDF documents to multiple-pages-per-sheet PDF documents, and allows you to insert lined and grid pages right beside your notes.

![upload.png](https://bitbucket.org/repo/Ljdbdk/images/850792518-upload.png)

### License ###

The source code is licensed under GPLv2. See LICENSE for more info.

### Setup ###

#### MakeNoteGUI (recommended) ####

1. Ensure that you have the Java Runtime Environment installed. If you do not, you can get it from [here](https://www.java.com/en/download/manual.jsp).
2. Download [MakeNoteGUI.zip](https://bitbucket.org/aim25/makenote/downloads/MakeNoteGUI.zip) from the repository.
3. Unzip MakeNoteGUI.zip.
4. Double-click on MakeNoteGUI.jar.
5. Please see "Graphics User Interface Usage" below for instructions on how to use the program.

Note: some people have reported issues when placing the contents of the zip under "C:\Program Files (x86)" in Windows, so it is recommended that you place the contents somewhere else.

#### MakeNote Command Line Utility ####

1. Ensure that you have the Java Runtime Environment installed. If you do not, you can get it from [here](https://www.java.com/en/download/manual.jsp).
1. Download [MakeNote.zip](https://bitbucket.org/aim25/makenote/downloads/MakeNote.zip) from the repository.
3. Unzip MakeNote.zip.
4. Run MakeNote.jar from the terminal.
5. Please see "Command Line Usage" below for instructions on how to use the program.

### Graphical User Interface Usage ###

1. Select a file or folder for as the input path. If a folder is chosen, then all PDF files directly under that folder will be converted. If a file is chosen, then only that file will be converted.
2. (Optional) You may select an output path for the document. If you do not select one, then the output document(s) will be saved to the same folder as the input path with a different name (i.e. your original file will not be overwritten).
3. (Optional) If you have a custom configuration saved to a file, or would like to save the current configuration to a file then you can use "Load Config" and "Save Config", respectively, to do so.
4. Either input your own custom settings into the options or choose from one of the presets on the right.
5. Press the "Generate" button to being conversion.

### Command Line Usage ###

```
#!
Usage: java -jar MakeNote [OPTIONS] input_path

  -outpath -f         Absolute or relative file location of the rendered output.
  
  -config -c          Plain text file with arguments.
  
  -size -s            Set the page size. (Default: LETTER)
                        LETTER
                        LEGAL
  
  -orientation -o     The page orientation of the output file. (Default: LANDSCAPE)
                        PORTRAIT
                        LANDSCAPE
  
  -dpi -d             DPI used for rendering. (Default: 200)
  
  -xseg -x            Number of horizontal segments. (Default: 2)
  
  -yseg -y            Number of vertical segments. (Default: 2)
  
  -pattern -p         Pattern indicating what each segment of the note should 
                      be. The number of characters in the pattern must be equal
                      to (xseg * yseg). The pattern starts at the top-left and
                      goes across and then down. (Default: NLNL)
                        N - A page from the note
                        L - A lined page (1/4 inch spacing)
                        G - A grid page (1/4 inch spacing)
                        B - A blank page
  
  -outline -l         Disables outline for each segment.
  
  -linespacing -ls    Line spacing for line and grid segments in inches. (Default: 0.25)
```

### Configuration File ###

Command line arguments can be saved to text files for easy access.

1. Create new text file.
2. Insert the arguments onto one line.
3. Run MakeNote.jar from the terminal using the [-config file] argument (for usage example see "Command Line Usage Examples" below).

### Command Line Usage Examples ###

* Default settings

```
#!
java -jar MakeNote.jar /path/to/file.pdf
```

* 2x2 with grid paper with spacing 1/8 inch

```
#!
java -jar MakeNote.jar -pattern NGNG -linespacing 0.125 /path/to/file.pdf
```

* 2x1 with lined paper

```
#!
java -jar MakeNote.jar -xseg 2 -yseg 1 -pattern NL /path/to/file.pdf
```

* Loading settings from configuration file

```
#!
java -jar MakeNote.jar -c /path/to/config/file.txt /path/to/file.pdf
```

### Contributing ###

If you would like to contribute, please fork the project and commit back your enhancements.