/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

abstract class Utils {
    public static FilenameFilter pdfFilter = new FilenameFilter() {
        @Override
        public boolean accept(File dir, String name) {
            return name.toLowerCase().endsWith(".pdf");
        }
    };

    public static BufferedImage scaleImage(BufferedImage inputImg, double scaleFactor) {
        // Return input image if scale factor is less then or equal to 0, or
        // equal to 1.
        if (scaleFactor == 1 || scaleFactor <= 0) {
            return inputImg;
        }

        // Scale image.
        int scaledWidth = (int) Math.floor(scaleFactor * inputImg.getWidth());
        int scaledHeight = (int) Math.floor(scaleFactor * inputImg.getHeight());
        Image scaledImage = inputImg.getScaledInstance(scaledWidth, scaledHeight, BufferedImage.SCALE_SMOOTH);

        // Convert Image to BufferedImage.
        BufferedImage outputImg = new BufferedImage(scaledWidth, scaledHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2D = outputImg.createGraphics();
        g2D.drawImage(scaledImage, 0, 0, null);

        // Free resources.
        scaledImage.flush();
        g2D.dispose();

        // Return scaled image.
        return outputImg;
    }

    public static Config parseArguments(String[] inputArgs) throws IllegalArgumentException, IOException {
        // Command line argument flags.
        final String[] OUTPUT = {"-outfile", "-f"};
        final String[] CONFIG = {"-config", "-c"};
        final String[] SIZE = {"-size", "-s"};
        final String[] ORIENT = {"-orientation", "-o"};
        final String[] DPI = {"-dpi", "-d"};
        final String[] XSEG = {"-xseg", "-x"};
        final String[] YSEG = {"-yseg", "-y"};
        final String[] PATTERN = {"-pattern", "-p"};
        final String[] OUTLINE = {"-outline", "-l"};
        final String[] LINESPACE = {"-linespacing", "-ls"};
        final String[] HELP = {"-help", "-h"};

        // Create a temporary copy of the input arguments.
        String[] tempArgs = new String[inputArgs.length];
        System.arraycopy(inputArgs, 0, tempArgs, 0, inputArgs.length);

        // Create a default Config object and adjust it based on the input
        // arguments.
        Config config = new Config();

        boolean isConfigFileLoaded = false;

        while (true) {
            for (int i = 0; i < tempArgs.length; i++) {
                if (tempArgs[i].equals(OUTPUT[0]) || tempArgs[i].equals(OUTPUT[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setOut(tempArgs[i]);
                } else if (tempArgs[i].equals(CONFIG[0]) || tempArgs[i].equals(CONFIG[1])) {
                    if (isConfigFileLoaded) {
                        continue;
                    }

                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setConfigFile(tempArgs[i]);
                } else if (tempArgs[i].equals(SIZE[0]) || tempArgs[i].equals(SIZE[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    if (tempArgs[i].toUpperCase().equals("LETTER")) {
                        config.setPageSize(PageSize.LETTER);
                    } else if (tempArgs[i].toUpperCase().equals("LEGAL")) {
                        config.setPageSize(PageSize.LEGAL);
                    } else if (tempArgs[i].toUpperCase().equals("A4")) {
                        config.setPageSize(PageSize.A4);
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else if (tempArgs[i].equals(ORIENT[0]) || tempArgs[i].equals(ORIENT[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    if (tempArgs[i].toUpperCase().equals("PORTRAIT")) {
                        config.setOrientation(PageOrientation.PORTRAIT);
                    } else if (tempArgs[i].toUpperCase().equals("LANDSCAPE")) {
                        config.setOrientation(PageOrientation.LANDSCAPE);
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else if (tempArgs[i].equals(DPI[0]) || tempArgs[i].equals(DPI[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setDpi(Integer.parseInt(tempArgs[i]));
                } else if (tempArgs[i].equals(XSEG[0]) || tempArgs[i].equals(XSEG[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setXseg(Integer.parseInt(tempArgs[i]));
                } else if (tempArgs[i].equals(YSEG[0]) || tempArgs[i].equals(YSEG[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setYseg(Integer.parseInt(tempArgs[i]));
                } else if (tempArgs[i].equals(PATTERN[0]) || tempArgs[i].equals(PATTERN[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setPattern(tempArgs[i]);
                } else if (tempArgs[i].equals(OUTLINE[0]) || tempArgs[i].equals(OUTLINE[1])) {
                    config.setOutlineEnabled(false);
                } else if (tempArgs[i].equals(LINESPACE[0]) || tempArgs[i].equals(LINESPACE[1])) {
                    if (++i >= tempArgs.length) {
                        throw new IllegalArgumentException();
                    }

                    config.setSpacingInch(Double.parseDouble(tempArgs[i]));
                } else if (tempArgs[i].equals(HELP[0]) || tempArgs[i].equals(HELP[1])) {
                    throw new IllegalArgumentException();
                } else {
                    if (config.getIn() == null) {
                        config.setIn(tempArgs[i]);
                    }
                }
            }

            // If a config file was given then load its contents in as command
            // line arguments and re-parse.
            if (config.getConfigFile() != null) {
                List<String> configLine = Files.readAllLines(Paths.get(config.getConfigFile()));
                tempArgs = configLine.get(0).split(" ");
                isConfigFileLoaded = true;
                config.setConfigFile(null);
                continue;
            }

            break;
        }

        return config;
    }

    public static String generateOutputPath(String inputPath) {
        int fileNameIndex = inputPath.lastIndexOf(FileSystems.getDefault().getSeparator()) + 1;
        return inputPath.substring(0, fileNameIndex) + "MakeNote_" + inputPath.substring(fileNameIndex);
    }

    public static Config fileToConfig(File configFile, Config config) throws IOException {
        // Read file contents into string.
        BufferedReader br = new BufferedReader(new FileReader(configFile));
        String line = br.readLine();

        String fileContents = new String();
        while (line != null) {
            fileContents += line;
            line = br.readLine();
        }
        fileContents.replace('\n', ' ');

        // Explode file contents into individual parts.
        String[] inputArgs = fileContents.split(" ");

        // Free resources.
        br.close();

        // Parse input arguments into Config.
        return Utils.parseArguments(inputArgs);
    }

    public static void configToFile(Config config, File configFile) throws IOException {
        // @formatter:off
        String argsString = "-s " + config.getSize().toString() +
                            " -o " + config.getOrientation().toString() +
                            " -d " + config.getDpi() +
                            " -x " + config.getXseg() +
                            " -y " + config.getYseg() +
                            " -p " + config.getPattern() +
                            " -l " + config.isOutlineEnabled() +
                            " -ls " + config.getSpacingInch();
        // @formatter:on
        
        if (!configFile.exists()) {
            configFile.createNewFile();
        }

        BufferedWriter bw = new BufferedWriter(new FileWriter(configFile));
        bw.write(argsString);
        bw.close();
    }
}