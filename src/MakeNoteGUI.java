/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;
import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

public class MakeNoteGUI {
    private JFrame frmMakenote;
    private JMenuBar menuBar;
    private JPanel pnlInFile;
    private JLabel lblInputFile;
    private JTextField txtInFilePath;
    private JButton btnBrowseIn;
    private JPanel pnlOutFile;
    private JLabel lblOutputFile;
    private JTextField txtOutFilePath;
    private JButton btnBrowseOut;
    private JSeparator serparator_1;
    private JPanel pnlConfigFile;
    private JLabel lblConfigFile;
    private JButton btnLoadConfig;
    private JButton btnSaveConfig;
    private JPanel pnlOptions;
    private JLabel lblOptions;
    private JLabel lblSize;
    private JRadioButton rdbtnLetter;
    private JRadioButton rdbtnLegal;
    private JRadioButton rdbtnA4;
    private ButtonGroup btngSize;
    private JRadioButton rdbtnLandscape;
    private JLabel lblOrientation;
    private JRadioButton rdbtnPortrait;
    private ButtonGroup btngOrientation;
    private JLabel lblDpi;
    private JTextField txtDpi;
    private JLabel lblPagesPerSheet;
    private JTextField txtX;
    private JLabel lblX;
    private JTextField txtY;
    private JLabel lblPattern;
    private JTextField txtPattern;
    private JLabel lblOutline;
    private JRadioButton rdbtnOutlineEnabled;
    private JRadioButton rdbtnOutlineDisabled;
    private ButtonGroup btngOutline;
    private JLabel lblLineSpacing;
    private JTextField txtLineSpacing;
    private JSeparator separator_2;
    private JButton btnGenerate;
    private JSeparator separator_4;
    private JPanel pnlPresets;
    private JLabel lblPresets;
    private JLabel lblPresetImg1;
    private JRadioButton rdbtnPreset1;
    private JLabel lblPresetImg2;
    private JRadioButton rdbtnPreset2;
    private JLabel lblPresetImg3;
    private JRadioButton rdbtnPreset3;
    private JLabel lblPresetImg4;
    private JRadioButton rdbtnPreset4;
    private JLabel lblPresetImg5;
    private JRadioButton rdbtnPreset5;
    private JLabel lblPresetImg6;
    private JRadioButton rdbtnPreset6;
    private JLabel lblPresetImg7;
    private JRadioButton rdbtnPreset7;
    private JRadioButton rdbtnPreset8;
    private JLabel lblPresetImg8;
    private ButtonGroup btngPreset;
    private OptionsActionListener optionsActionListener;

    private Config config;

    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (Throwable e) {
            e.printStackTrace();
        }
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MakeNoteGUI window = new MakeNoteGUI();
                    window.frmMakenote.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public MakeNoteGUI() throws IOException {
        config = new Config();
        initialize();
    }

    private void initialize() throws IOException {
        frmMakenote = new JFrame();
        frmMakenote.setResizable(false);
        frmMakenote.setTitle("MakeNote");
        frmMakenote.setBounds(100, 100, 700, 727);
        frmMakenote.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frmMakenote.getContentPane().setLayout(null);

        menuBar = new JMenuBar();
        menuBar.setBounds(0, 0, 694, 21);
        frmMakenote.getContentPane().add(menuBar);

        pnlInFile = new JPanel();
        pnlInFile.setBounds(10, 32, 296, 34);
        frmMakenote.getContentPane().add(pnlInFile);
        pnlInFile.setLayout(null);

        lblInputFile = new JLabel("Input Path:");
        lblInputFile.setFont(new Font("Arial", Font.BOLD, 12));
        lblInputFile.setBounds(0, 9, 60, 14);
        pnlInFile.add(lblInputFile);

        txtInFilePath = new JTextField();
        txtInFilePath.setFont(new Font("Arial", Font.PLAIN, 12));
        txtInFilePath.setBounds(80, 6, 124, 22);
        txtInFilePath.setColumns(10);
        pnlInFile.add(txtInFilePath);

        btnBrowseIn = new JButton("Browse...");
        btnBrowseIn.addActionListener(new BtnBrowseInActionListener());
        btnBrowseIn.setFont(new Font("Arial", Font.PLAIN, 12));
        btnBrowseIn.setBounds(213, 5, 83, 23);
        pnlInFile.add(btnBrowseIn);

        pnlOutFile = new JPanel();
        pnlOutFile.setLayout(null);
        pnlOutFile.setBounds(10, 65, 296, 34);
        frmMakenote.getContentPane().add(pnlOutFile);

        lblOutputFile = new JLabel("Output Path:");
        lblOutputFile.setFont(new Font("Arial", Font.BOLD, 12));
        lblOutputFile.setBounds(0, 9, 70, 14);
        pnlOutFile.add(lblOutputFile);

        txtOutFilePath = new JTextField();
        txtOutFilePath.setFont(new Font("Arial", Font.PLAIN, 12));
        txtOutFilePath.setColumns(10);
        txtOutFilePath.setBounds(80, 6, 123, 22);
        pnlOutFile.add(txtOutFilePath);

        btnBrowseOut = new JButton("Browse...");
        btnBrowseOut.addActionListener(new BtnBrowseOutActionListener());
        btnBrowseOut.setFont(new Font("Arial", Font.PLAIN, 12));
        btnBrowseOut.setBounds(213, 5, 83, 23);
        pnlOutFile.add(btnBrowseOut);

        serparator_1 = new JSeparator();
        serparator_1.setForeground(Color.BLACK);
        serparator_1.setBounds(10, 110, 296, 2);
        frmMakenote.getContentPane().add(serparator_1);

        pnlConfigFile = new JPanel();
        pnlConfigFile.setLayout(null);
        pnlConfigFile.setBounds(10, 122, 296, 34);
        frmMakenote.getContentPane().add(pnlConfigFile);

        lblConfigFile = new JLabel("Config File:");
        lblConfigFile.setFont(new Font("Arial", Font.BOLD, 12));
        lblConfigFile.setBounds(0, 9, 63, 14);
        pnlConfigFile.add(lblConfigFile);

        btnLoadConfig = new JButton("Load Config");
        btnLoadConfig.addActionListener(new BtnLoadConfigActionListener());
        btnLoadConfig.setFont(new Font("Arial", Font.PLAIN, 12));
        btnLoadConfig.setBounds(73, 5, 105, 23);
        pnlConfigFile.add(btnLoadConfig);

        btnSaveConfig = new JButton("Save Config");
        btnSaveConfig.addActionListener(new BtnSaveConfigActionListener());
        btnSaveConfig.setFont(new Font("Arial", Font.PLAIN, 12));
        btnSaveConfig.setBounds(191, 5, 105, 23);
        pnlConfigFile.add(btnSaveConfig);

        pnlOptions = new JPanel();
        pnlOptions.setBounds(10, 167, 296, 210);
        frmMakenote.getContentPane().add(pnlOptions);
        pnlOptions.setLayout(null);

        lblOptions = new JLabel("Options:");
        lblOptions.setFont(new Font("Arial", Font.BOLD, 12));
        lblOptions.setBounds(0, 0, 56, 14);
        pnlOptions.add(lblOptions);

        lblSize = new JLabel("Size:");
        lblSize.setFont(new Font("Arial", Font.PLAIN, 12));
        lblSize.setBounds(10, 25, 35, 14);
        pnlOptions.add(lblSize);

        rdbtnLetter = new JRadioButton("Letter");
        rdbtnLetter.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnLetter.setBounds(117, 20, 56, 23);
        pnlOptions.add(rdbtnLetter);

        rdbtnLegal = new JRadioButton("Legal");
        rdbtnLegal.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnLegal.setBounds(175, 20, 56, 23);
        pnlOptions.add(rdbtnLegal);

        rdbtnA4 = new JRadioButton("A4");
        rdbtnA4.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnA4.setBounds(233, 20, 56, 23);
        pnlOptions.add(rdbtnA4);

        btngSize = new ButtonGroup();
        btngSize.add(rdbtnLetter);
        btngSize.add(rdbtnLegal);
        btngSize.add(rdbtnA4);

        lblOrientation = new JLabel("Orientation:");
        lblOrientation.setFont(new Font("Arial", Font.PLAIN, 12));
        lblOrientation.setBounds(10, 51, 68, 14);
        pnlOptions.add(lblOrientation);

        rdbtnLandscape = new JRadioButton("Landscape");
        rdbtnLandscape.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnLandscape.setBounds(117, 46, 87, 23);
        pnlOptions.add(rdbtnLandscape);

        rdbtnPortrait = new JRadioButton("Portrait");
        rdbtnPortrait.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnPortrait.setBounds(210, 46, 87, 23);
        pnlOptions.add(rdbtnPortrait);

        btngOrientation = new ButtonGroup();
        btngOrientation.add(rdbtnLandscape);
        btngOrientation.add(rdbtnPortrait);

        lblDpi = new JLabel("DPI:");
        lblDpi.setFont(new Font("Arial", Font.PLAIN, 12));
        lblDpi.setBounds(10, 76, 25, 14);
        pnlOptions.add(lblDpi);

        txtDpi = new JTextField();
        txtDpi.setHorizontalAlignment(SwingConstants.CENTER);
        txtDpi.setFont(new Font("Arial", Font.PLAIN, 12));
        txtDpi.setBounds(117, 73, 38, 20);
        pnlOptions.add(txtDpi);
        txtDpi.setColumns(10);

        lblPagesPerSheet = new JLabel("Pages per sheet:");
        lblPagesPerSheet.setFont(new Font("Arial", Font.PLAIN, 12));
        lblPagesPerSheet.setBounds(10, 101, 97, 14);
        pnlOptions.add(lblPagesPerSheet);

        txtX = new JTextField();
        txtX.setFont(new Font("Arial", Font.PLAIN, 12));
        txtX.setHorizontalAlignment(SwingConstants.CENTER);
        txtX.setBounds(117, 99, 25, 20);
        pnlOptions.add(txtX);
        txtX.setColumns(10);

        lblX = new JLabel("x");
        lblX.setHorizontalAlignment(SwingConstants.CENTER);
        lblX.setFont(new Font("Arial", Font.PLAIN, 12));
        lblX.setBounds(138, 101, 29, 14);
        pnlOptions.add(lblX);

        txtY = new JTextField();
        txtY.setFont(new Font("Arial", Font.PLAIN, 12));
        txtY.setHorizontalAlignment(SwingConstants.CENTER);
        txtY.setColumns(10);
        txtY.setBounds(163, 99, 25, 20);
        pnlOptions.add(txtY);

        lblPattern = new JLabel("Pattern:");
        lblPattern.setFont(new Font("Arial", Font.PLAIN, 12));
        lblPattern.setBounds(10, 129, 46, 14);
        pnlOptions.add(lblPattern);

        txtPattern = new JTextField();
        txtPattern.setFont(new Font("Arial", Font.PLAIN, 12));
        txtPattern.setColumns(10);
        txtPattern.setBounds(117, 126, 169, 20);
        pnlOptions.add(txtPattern);

        lblOutline = new JLabel("Outline:");
        lblOutline.setFont(new Font("Arial", Font.PLAIN, 12));
        lblOutline.setBounds(10, 155, 46, 14);
        pnlOptions.add(lblOutline);

        rdbtnOutlineEnabled = new JRadioButton("Enabled");
        rdbtnOutlineEnabled.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnOutlineEnabled.setBounds(117, 150, 71, 23);
        pnlOptions.add(rdbtnOutlineEnabled);

        rdbtnOutlineDisabled = new JRadioButton("Disabled");
        rdbtnOutlineDisabled.setFont(new Font("Arial", Font.PLAIN, 12));
        rdbtnOutlineDisabled.setBounds(191, 151, 75, 23);
        pnlOptions.add(rdbtnOutlineDisabled);

        btngOutline = new ButtonGroup();
        btngOutline.add(rdbtnOutlineEnabled);
        btngOutline.add(rdbtnOutlineDisabled);

        lblLineSpacing = new JLabel("Line spacing:");
        lblLineSpacing.setFont(new Font("Arial", Font.PLAIN, 12));
        lblLineSpacing.setBounds(10, 183, 80, 14);
        pnlOptions.add(lblLineSpacing);

        txtLineSpacing = new JTextField();
        txtLineSpacing.setHorizontalAlignment(SwingConstants.CENTER);
        txtLineSpacing.setFont(new Font("Arial", Font.PLAIN, 12));
        txtLineSpacing.setColumns(10);
        txtLineSpacing.setBounds(117, 180, 38, 20);
        pnlOptions.add(txtLineSpacing);

        separator_2 = new JSeparator();
        separator_2.setForeground(Color.BLACK);
        separator_2.setBounds(10, 388, 296, 2);
        frmMakenote.getContentPane().add(separator_2);

        btnGenerate = new JButton("Generate");
        btnGenerate.addActionListener(new BtnGenerateActionListener());
        btnGenerate.setFont(new Font("Arial", Font.PLAIN, 12));
        btnGenerate.setBounds(10, 400, 296, 34);
        frmMakenote.getContentPane().add(btnGenerate);

        separator_4 = new JSeparator();
        separator_4.setOrientation(SwingConstants.VERTICAL);
        separator_4.setForeground(Color.BLACK);
        separator_4.setBounds(316, 32, 2, 654);
        frmMakenote.getContentPane().add(separator_4);

        pnlPresets = new JPanel();
        pnlPresets.setBounds(326, 32, 358, 654);
        frmMakenote.getContentPane().add(pnlPresets);
        pnlPresets.setLayout(null);

        lblPresets = new JLabel("Presets:");
        lblPresets.setFont(new Font("Arial", Font.BOLD, 12));
        lblPresets.setBounds(0, 0, 48, 14);
        pnlPresets.add(lblPresets);

        optionsActionListener = new OptionsActionListener();

        lblPresetImg1 = new JLabel("");
        lblPresetImg1.setBounds(10, 20, 165, 135);
        lblPresetImg1.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset1.jpg"))));
        pnlPresets.add(lblPresetImg1);

        rdbtnPreset1 = new JRadioButton("2x2 Landscape NLNL");
        rdbtnPreset1.addActionListener(optionsActionListener);
        rdbtnPreset1.setSelected(true);
        rdbtnPreset1.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset1.setBounds(10, 156, 165, 14);
        pnlPresets.add(rdbtnPreset1);

        lblPresetImg2 = new JLabel("");
        lblPresetImg2.setBounds(185, 20, 165, 135);
        lblPresetImg2.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset2.jpg"))));
        pnlPresets.add(lblPresetImg2);

        rdbtnPreset2 = new JRadioButton("2x2 Landscape NGNG");
        rdbtnPreset2.addActionListener(optionsActionListener);
        rdbtnPreset2.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset2.setBounds(185, 156, 165, 14);
        pnlPresets.add(rdbtnPreset2);

        lblPresetImg3 = new JLabel("");
        lblPresetImg3.setBounds(10, 175, 165, 135);
        lblPresetImg3.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset3.jpg"))));
        pnlPresets.add(lblPresetImg3);

        rdbtnPreset3 = new JRadioButton("2x1 Landscape NL");
        rdbtnPreset3.addActionListener(optionsActionListener);
        rdbtnPreset3.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset3.setBounds(10, 311, 165, 14);
        pnlPresets.add(rdbtnPreset3);

        lblPresetImg4 = new JLabel("");
        lblPresetImg4.setBounds(185, 175, 165, 135);
        lblPresetImg4.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset4.jpg"))));
        pnlPresets.add(lblPresetImg4);

        rdbtnPreset4 = new JRadioButton("2x1 Landscape NG");
        rdbtnPreset4.addActionListener(optionsActionListener);
        rdbtnPreset4.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset4.setBounds(185, 311, 165, 14);
        pnlPresets.add(rdbtnPreset4);

        lblPresetImg5 = new JLabel("");
        lblPresetImg5.setBounds(10, 330, 165, 135);
        lblPresetImg5.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset5.jpg"))));
        pnlPresets.add(lblPresetImg5);

        rdbtnPreset5 = new JRadioButton("2x2 Portrait NLNL");
        rdbtnPreset5.addActionListener(optionsActionListener);
        rdbtnPreset5.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset5.setBounds(10, 466, 165, 14);
        pnlPresets.add(rdbtnPreset5);

        lblPresetImg6 = new JLabel("");
        lblPresetImg6.setBounds(185, 330, 165, 135);
        lblPresetImg6.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset6.jpg"))));
        pnlPresets.add(lblPresetImg6);

        rdbtnPreset6 = new JRadioButton("2x2 Portrait NGNG");
        rdbtnPreset6.addActionListener(optionsActionListener);
        rdbtnPreset6.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset6.setBounds(185, 466, 165, 14);
        pnlPresets.add(rdbtnPreset6);

        lblPresetImg7 = new JLabel("");
        lblPresetImg7.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset7.jpg"))));
        lblPresetImg7.setBounds(10, 485, 165, 135);
        pnlPresets.add(lblPresetImg7);

        rdbtnPreset7 = new JRadioButton("2x3 Portrait NLNLNL");
        rdbtnPreset7.addActionListener(optionsActionListener);
        rdbtnPreset7.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset7.setBounds(10, 621, 165, 14);
        pnlPresets.add(rdbtnPreset7);

        lblPresetImg8 = new JLabel("");
        lblPresetImg8.setBounds(185, 485, 165, 135);
        lblPresetImg8.setIcon(new ImageIcon(ImageIO.read(getClass().getResourceAsStream("preset8.jpg"))));
        pnlPresets.add(lblPresetImg8);

        rdbtnPreset8 = new JRadioButton("2x3 Portrait NGNGNG");
        rdbtnPreset8.addActionListener(optionsActionListener);
        rdbtnPreset8.setHorizontalAlignment(SwingConstants.CENTER);
        rdbtnPreset8.setBounds(185, 621, 165, 14);
        pnlPresets.add(rdbtnPreset8);

        btngPreset = new ButtonGroup();
        btngPreset.add(rdbtnPreset1);
        btngPreset.add(rdbtnPreset2);
        btngPreset.add(rdbtnPreset3);
        btngPreset.add(rdbtnPreset4);
        btngPreset.add(rdbtnPreset5);
        btngPreset.add(rdbtnPreset6);
        btngPreset.add(rdbtnPreset7);
        btngPreset.add(rdbtnPreset8);

        updateOptionsPnl(config);
    }

    private class BtnBrowseInActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            int fileChooseReturn = fileChooser.showOpenDialog(frmMakenote);

            if (fileChooseReturn == JFileChooser.APPROVE_OPTION) {
                txtInFilePath.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }

    private class BtnBrowseOutActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
            int fileChooseReturn = fileChooser.showSaveDialog(frmMakenote);

            if (fileChooseReturn == JFileChooser.APPROVE_OPTION) {
                txtOutFilePath.setText(fileChooser.getSelectedFile().getAbsolutePath());
            }
        }
    }

    private class BtnLoadConfigActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            int fileChooseReturn = fileChooser.showOpenDialog(frmMakenote);

            if (fileChooseReturn == JFileChooser.APPROVE_OPTION) {
                try {
                    updateOptionsPnl(Utils.fileToConfig(fileChooser.getSelectedFile(), config));
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(frmMakenote, "Could not load configuration file.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private class BtnSaveConfigActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            JFileChooser fileChooser = new JFileChooser();
            int fileChooseReturn = fileChooser.showSaveDialog(frmMakenote);

            if (fileChooseReturn == JFileChooser.APPROVE_OPTION) {
                try {
                    updateConfigFromOptions(config);
                    Utils.configToFile(config, fileChooser.getSelectedFile());
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(frmMakenote, "Could not save configuration file.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private class OptionsActionListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            config.reset();

            if (e.getSource().equals(rdbtnPreset1)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.LANDSCAPE);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(2);
                config.setPattern("NLNL");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset2)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.LANDSCAPE);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(2);
                config.setPattern("NGNG");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset3)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.LANDSCAPE);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(1);
                config.setPattern("NL");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset4)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.LANDSCAPE);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(1);
                config.setPattern("NG");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset5)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.PORTRAIT);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(2);
                config.setPattern("NLNL");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset6)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.PORTRAIT);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(2);
                config.setPattern("NGNG");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset7)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.PORTRAIT);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(3);
                config.setPattern("NLNLNL");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            } else if (e.getSource().equals(rdbtnPreset8)) {
                config.setPageSize(PageSize.LETTER);
                config.setOrientation(PageOrientation.PORTRAIT);
                config.setDpi(200);
                config.setXseg(2);
                config.setYseg(3);
                config.setPattern("NGNGNG");
                config.setOutlineEnabled(true);
                config.setSpacingInch(0.25);
            }

            updateOptionsPnl(config);
        }
    }

    private class BtnGenerateActionListener implements ActionListener {
        public void actionPerformed(ActionEvent arg0) {
            // Prepare config by loading values from the various fields.
            updateConfigFromOptions(config);
            config.setIn(txtInFilePath.getText());
            config.setOut(txtOutFilePath.getText());

            if (config.getIn().isEmpty()) {
                JOptionPane.showMessageDialog(frmMakenote, "No input file specified.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            // Initialize all file system resources.
            File inPath = new File(config.getIn());

            if (!inPath.exists()) {
                JOptionPane.showMessageDialog(frmMakenote, "Input file " + config.getIn() + "does not exist.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (!inPath.canRead()) {
                JOptionPane.showMessageDialog(frmMakenote, "Cannot read input file " + config.getIn() + ".", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            if (config.getOut().isEmpty()) {
                if (inPath.isFile()) {
                    config.setOut(Utils.generateOutputPath(inPath.getAbsolutePath()));
                } else if (inPath.isDirectory()) {
                    config.setOut(inPath.getAbsolutePath());
                } else {
                    JOptionPane.showMessageDialog(frmMakenote, "Could not resolve input file/directory.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            File outPath = new File(config.getOut());

            if (inPath.isFile() && outPath.isDirectory()) {
                outPath = new File(Utils.generateOutputPath(outPath.getAbsolutePath() + FileSystems.getDefault().getSeparator() + inPath.getName()));
            }

            if (inPath.isDirectory() && outPath.isFile()) {
                JOptionPane.showMessageDialog(frmMakenote, "Output path must be a directory.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }

            List<File> pdfFilesIn = new LinkedList<File>();
            List<File> pdfFilesOut = new LinkedList<File>();

            if (inPath.isFile()) {
                pdfFilesIn.add(inPath);
                pdfFilesOut.add(outPath);
            } else if (inPath.isDirectory()) {
                for (File f : inPath.listFiles(Utils.pdfFilter)) {
                    pdfFilesIn.add(f);
                    pdfFilesOut.add(new File(Utils.generateOutputPath(outPath.getAbsolutePath() + FileSystems.getDefault().getSeparator() + f.getName())));
                }
            } else {
                JOptionPane.showMessageDialog(frmMakenote, "Could not resolve input file/directory.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            // Check arguments.
            if (config.getDpi() <= 72) {
                JOptionPane.showMessageDialog(frmMakenote, "DPI must be at least 72.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            if (config.getXseg() <= 0 || config.getYseg() <= 0) {
                JOptionPane.showMessageDialog(frmMakenote, "Pages per sheet must multiply to greater than 0.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            if (config.getPattern().length() != config.getXseg() * config.getYseg()) {
                JOptionPane.showMessageDialog(frmMakenote, "Pattern length does not match number of segments.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            if (config.getSpacingInch() <= 0) {
                JOptionPane.showMessageDialog(frmMakenote, "Spacing must be greater than 0.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                return;
            }
            
            // Display working dialog.
            WorkingDialog dialog = new WorkingDialog(config, pdfFilesIn, pdfFilesOut, frmMakenote);
            dialog.setVisible(true);
        }
    }

    private void updateOptionsPnl(Config config) {
        if (config.getSize().equals(PageSize.LETTER)) {
            rdbtnLetter.setSelected(true);
        } else if (config.getSize().equals(PageSize.LEGAL)) {
            rdbtnLegal.setSelected(true);
        } else if (config.getSize().equals(PageSize.A4)) {
            rdbtnA4.setSelected(true);
        }

        if (config.getOrientation().equals(PageOrientation.LANDSCAPE)) {
            rdbtnLandscape.setSelected(true);
        } else if (config.getOrientation().equals(PageOrientation.PORTRAIT)) {
            rdbtnPortrait.setSelected(true);
        }

        txtDpi.setText(Integer.toString(config.getDpi()));

        txtX.setText(Integer.toString(config.getXseg()));
        txtY.setText(Integer.toString(config.getYseg()));

        txtPattern.setText(config.getPattern());

        if (config.isOutlineEnabled()) {
            rdbtnOutlineEnabled.setSelected(true);
        } else {
            rdbtnOutlineDisabled.setSelected(true);
        }

        txtLineSpacing.setText(Double.toString(config.getSpacingInch()));
    }

    private void updateConfigFromOptions(Config config) {
        if (rdbtnLetter.isSelected()) {
            config.setPageSize(PageSize.LETTER);
        } else if (rdbtnA4.isSelected()) {
            config.setPageSize(PageSize.A4);
        } else {
            config.setPageSize(PageSize.LEGAL);
        }

        if (rdbtnLandscape.isSelected()) {
            config.setOrientation(PageOrientation.LANDSCAPE);
        } else {
            config.setOrientation(PageOrientation.PORTRAIT);
        }

        config.setDpi(Integer.parseInt(txtDpi.getText()));

        config.setXseg(Integer.parseInt(txtX.getText()));
        config.setYseg(Integer.parseInt(txtY.getText()));

        config.setPattern(txtPattern.getText());

        if (rdbtnOutlineEnabled.isSelected()) {
            config.setOutlineEnabled(true);
        } else {
            config.setOutlineEnabled(false);
        }

        config.setSpacingInch(Double.parseDouble(txtLineSpacing.getText()));
    }
}