/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDJpeg;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;

class NoteTemplate {
    private int horizontalSegments;
    private int verticalSegments;

    private PageSize size;
    private PageOrientation orientation;
    private int dpi;

    private String pattern;

    private int pageWidth;
    private int pageHeight;
    
    private List<Segment> template;
    
    public NoteTemplate(Config config) {
        horizontalSegments = config.getXseg();
        verticalSegments = config.getYseg();

        size = config.getSize();
        orientation = config.getOrientation();
        dpi = config.getDpi();

        pattern = config.getPattern();
        
        // Check if pattern matches vertical/horizontal configuration.
        if (pattern.length() != (horizontalSegments * verticalSegments)) {
            throw new IllegalArgumentException("Pattern lenght does not match number of segments.");
        }
        
        // Compute page/segment dimensions.
        pageWidth = (int) Math.floor(dpi * size.getWidth());
        pageHeight = (int) Math.floor(dpi * size.getHeight());

        if (orientation.equals(PageOrientation.LANDSCAPE)) {
            int temp = pageWidth;
            pageWidth = pageHeight;
            pageHeight = temp;
        }

        int segmentWidth = pageWidth / horizontalSegments;
        int segmentHeight = pageHeight / verticalSegments;
        
        // Setup template based on pattern.
        template = new LinkedList<Segment>();
        
        char[] explodePattern = pattern.toUpperCase().toCharArray();
        for (int i = 0; i < verticalSegments; i++) {
            for (int j = 0; j < horizontalSegments; j++) {
                int pos = (i * horizontalSegments) + j;

                if (explodePattern.length < pos) {
                    break;
                }

                if (explodePattern[pos] == 'N') {
                    template.add(new NoteSegment(segmentWidth, segmentHeight, j * segmentWidth, i * segmentHeight, config));
                } else if (explodePattern[pos] == 'L') {
                    template.add(new LinedSegment(segmentWidth, segmentHeight, j * segmentWidth, i * segmentHeight, config));
                } else if (explodePattern[pos] == 'G') {
                    template.add(new GridSegment(segmentWidth, segmentHeight, j * segmentWidth, i * segmentHeight, config));
                } else if (explodePattern[pos] == 'B') {
                    template.add(new BlankSegment(segmentWidth, segmentHeight, j * segmentWidth, i * segmentHeight, config));
                }
            }
        }
    }
    
    public PDDocument generateNoteFromTemplate(PDDocument inputDoc) throws IOException {
        // Load input document pages.
        @SuppressWarnings("unchecked")
        LinkedList<PDPage> inputPages = new LinkedList<PDPage>(inputDoc.getDocumentCatalog().getAllPages());

        // Create output document.
        PDDocument outputDoc = new PDDocument();

        // Render one page based on the template at a time.
        BufferedImage outputPageImg = new BufferedImage(pageWidth, pageHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2D = outputPageImg.createGraphics();
        
        boolean finished = false;
        while (true) {
            // Render all segments onto outputPageImg.
            for (Segment s : template) {
                // Initialize if NoteSegment.
                if (s.getClass().getName().equals("NoteSegment")) {
                    if (inputPages.size() == 0) {
                        finished = true;
                        break;
                    }
                    ((NoteSegment) s).setPage(inputPages.pop());
                }

                // Draw segment onto page.
                BufferedImage renderedPage = s.render();
                g2D.drawImage(renderedPage, s.getXpos(), s.getYpos(), null);
                renderedPage.flush();
            }

            // If all note pages have been rendered then exit.
            if (finished == true) {
                break;
            }

            // Create a page and add it to the outputDocument.
            PDPage outputPage = new PDPage(new PDRectangle(pageWidth, pageHeight));
            outputDoc.addPage(outputPage);

            // Draw outputPageImg to outputPage.
            PDXObjectImage xImg = new PDJpeg(outputDoc, outputPageImg, 1);
            PDPageContentStream content = new PDPageContentStream(outputDoc, outputPage);
            content.drawImage(xImg, 0, 0);
            content.close();
            xImg.clear();
        }

        g2D.dispose();
        outputPageImg.flush();

        // Return note generated to template specifications.
        return outputDoc;
    }
}