/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

class GridSegment extends Segment {
    private BufferedImage cachedPage;

    protected GridSegment(int width, int height, int xpos, int ypos, Config config) {
        super(width, height, xpos, ypos, config);

        cachedPage = null;
    }

    @Override
    public BufferedImage render() {
        // If rendered once, no need to render again.
        if (cachedPage != null) {
            return cachedPage;
        }

        // Create blank BufferedImage.
        BufferedImage bImg = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

        // Allocate stream resource.
        Graphics2D g2D = bImg.createGraphics();

        // Set background to white.
        g2D.setColor(getBgColor());
        g2D.fillRect(0, 0, getWidth(), getHeight());

        // If outline is enabled draw it.
        if (isOutline()) {
            g2D.setColor(getFgColor());
            g2D.drawRect(0, 0, getWidth(), getHeight());
        }

        // Draw grid.
        for (int i = 0; i < getHeight(); i += getSpacing()) {
            g2D.drawLine(0, i, getWidth(), i);
        }

        for (int i = 0; i < getWidth(); i += getSpacing()) {
            g2D.drawLine(i, 0, i, getHeight());
        }

        // Release stream resources.
        g2D.dispose();

        // Returned rendered page.
        cachedPage = bImg;
        return bImg;
    }
}