/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

class Config {
    public static final PageSize DEFAULT_PAGE_SIZE = PageSize.LETTER;
    public static final PageOrientation DEFAULT_ORIENTATION = PageOrientation.LANDSCAPE;
    public static final int DEFAULT_DPI = 200;
    public static final int DEFAULT_HORIZONTAL = 2;
    public static final int DEFAULT_VERTICAL = 2;
    public static final String DEFAULT_PATTERN = "NLNL";
    public static final boolean DEFAULT_OUTLINE = true;
    public static final double DEFAULT_SPACING_INCH = 0.25;

    private String in;
    private String out;
    private String configFile;
    private PageSize size;
    private PageOrientation orientation;
    private int dpi;
    private int xseg;
    private int yseg;
    private String pattern;
    private boolean outlineEnabled;
    private double spacingInch;
    private int spacingPx;

    public Config() {
        reset();
    }

    public void reset() {
        in = null;
        out = null;
        configFile = null;
        size = DEFAULT_PAGE_SIZE;
        orientation = DEFAULT_ORIENTATION;
        dpi = DEFAULT_DPI;
        xseg = DEFAULT_HORIZONTAL;
        yseg = DEFAULT_VERTICAL;
        pattern = DEFAULT_PATTERN;
        outlineEnabled = DEFAULT_OUTLINE;
        spacingInch = DEFAULT_SPACING_INCH;
        spacingPx = (int) Math.floor(DEFAULT_SPACING_INCH * DEFAULT_DPI);
    }

    public String getIn() {
        return in;
    }

    public void setIn(String in) {
        this.in = in;
    }

    public String getOut() {
        return out;
    }

    public void setOut(String out) {
        this.out = out;
    }

    public String getConfigFile() {
        return configFile;
    }

    public void setConfigFile(String configFile) {
        this.configFile = configFile;
    }

    public PageSize getSize() {
        return size;
    }

    public void setPageSize(PageSize size) {
        this.size = size;
    }

    public PageOrientation getOrientation() {
        return orientation;
    }

    public void setOrientation(PageOrientation orientation) {
        this.orientation = orientation;
    }

    public int getDpi() {
        return dpi;
    }

    public void setDpi(int dpi) {
        this.dpi = dpi;
        spacingPx = (int) Math.floor(spacingInch * this.dpi);
    }

    public int getXseg() {
        return xseg;
    }

    public void setXseg(int xseg) {
        this.xseg = xseg;
    }

    public int getYseg() {
        return yseg;
    }

    public void setYseg(int yseg) {
        this.yseg = yseg;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    public boolean isOutlineEnabled() {
        return outlineEnabled;
    }

    public void setOutlineEnabled(boolean outlineEnabled) {
        this.outlineEnabled = outlineEnabled;
    }

    public double getSpacingInch() {
        return spacingInch;
    }

    public void setSpacingInch(double spacingInch) {
        this.spacingInch = spacingInch;
        spacingPx = (int) Math.floor(spacingInch * dpi);
    }

    public int getSpacingPx() {
        return spacingPx;
    }

    public void setSpacingPx(int spacingPx) {
        this.spacingPx = spacingPx;
        spacingInch = Math.ceil(spacingPx / (double) dpi);
    }
}