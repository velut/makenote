/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;

public class MakeNote {
    public static void main(String[] args) {
        // Suppress logging messages.
        System.setProperty("org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");

        // Parse command line arguments.
        Config config = null;
        try {
            config = Utils.parseArguments(args);
        } catch (IllegalArgumentException iae) {
            System.err.println("Invalid usage.");
            usage();
            System.exit(0);
        } catch (IOException ioe) {
            System.err.println("Configuration could not be loaded from file.");
            System.exit(0);
        }

        if (config.getIn() == null) {
            System.err.println("No input path specified.");
            System.exit(0);
        }

        // Initialize all file system resources.
        File inPath = new File(config.getIn());

        if (!inPath.exists()) {
            System.err.println("Input file " + config.getIn() + "does not exist.");
            System.exit(0);
        }

        if (!inPath.canRead()) {
            System.err.println("Cannot read input file " + config.getIn() + ".");
            System.exit(0);
        }

        if (config.getOut() == null) {
            if (inPath.isFile()) {
                config.setOut(Utils.generateOutputPath(inPath.getAbsolutePath()));
            } else if (inPath.isDirectory()) {
                config.setOut(inPath.getAbsolutePath());
            }
        }
        File outPath = new File(config.getOut());

        if (inPath.isFile() && outPath.isDirectory()) {
            outPath = new File(Utils.generateOutputPath(outPath.getAbsolutePath() + FileSystems.getDefault().getSeparator() + inPath.getName()));
        }

        List<File> pdfFilesIn = new LinkedList<File>();
        List<File> pdfFilesOut = new LinkedList<File>();

        if (inPath.isFile()) {
            pdfFilesIn.add(inPath);
            pdfFilesOut.add(outPath);
        } else if (inPath.isDirectory()) {
            for (File f : inPath.listFiles(Utils.pdfFilter)) {
                pdfFilesIn.add(f);
                pdfFilesOut.add(new File(Utils.generateOutputPath(outPath.getAbsolutePath() + FileSystems.getDefault().getSeparator() + f.getName())));
            }
        } else {
            System.err.println("Could not resolve input file/directory.");
            System.exit(0);
        }

        // Print start message.
        System.out.println("Generating notes, please wait...");

        // Generate output documents.
        NoteTemplate noteTemplate = new NoteTemplate(config);
        PDDocument inDoc = null;
        PDDocument outDoc = null;
        try {
            for (int i = 0; i < pdfFilesIn.size(); i++) {
                printProgress(i + 1, pdfFilesIn.size());

                inDoc = PDDocument.load(pdfFilesIn.get(i));
                outDoc = noteTemplate.generateNoteFromTemplate(inDoc);
                outDoc.save(pdfFilesOut.get(i));

                inDoc.close();
                outDoc.close();
            }
        } catch (IOException ioe) {
            System.err.println("Error generating output document.");
            System.exit(0);
        } catch (COSVisitorException e) {
            System.err.println("Could not save output document.");
            System.exit(0);
        }

        // Print done message.
        System.out.println("\nDone! Output file(s) located at \"" + config.getOut() + "\"");
    }

    private static void usage() {
        // @formatter:off
        final String usageMessage = "\nUsage: java -jar MakeNote [OPTIONS] input_path\n\n" +
                                    "  -outpath -f         Absolute or relative file location of the rendered output.\n\n" +
                                    "  -config -c          Plain text file with arguments." +
                                    "  -size -s            Set the page size. (Default: LETTER).\n" +
                                    "                          LETTER\n" +
                                    "                          LEGAL\n" +
                                    "                          A4\n\n" +
                                    "  -orientation -o     The page orientation of the output file. (Default: LANDSCAPE)\n" +
                                    "                          PORTRAIT\n" +
                                    "                          LANDSCAPE\n\n" +
                                    "  -dpi -d             DPI used for rendering. (Default: 200)\n\n" +
                                    "  -xseg -x            Number of horizontal segments. (Default: 2)\n\n" +
                                    "  -yseg -y            Number of vertical segments. (Default: 2)\n\n" +
                                    "  -pattern -p         Pattern indicating what each segment of the note should be. The number of characters in the pattern must be equal to (xseg * yseg). The pattern starts at the top-left and goes across and then down. (Default: NLNL)\n" +
                                    "                          N - A page from the note\n" +
                                    "                          L - A lined page (1/4 inch spacing)\n" +
                                    "                          G - A grid page (1/4 inch spacing)\n" +
                                    "                          B - A blank page\n\n" +
                                    "  -outline -l         Disables outline for each segment.\n\n" +
                                    "  -linespacing -ls    Line spacing for line and grid segments in inches. (Default: 0.25)\n";
        // @formatter:on

        System.err.println(usageMessage);
    }

    private static void printProgress(int current, int total) {
        char[] bar = new char[total];

        for (int i = 0; i < current; i++) {
            bar[i] = '#';
        }

        for (int i = current; i < total; i++) {
            bar[i] = ' ';
        }

        String progress = "\rProgress [" + (new String(bar)) + "]";
        System.out.print(progress);
    }
}