import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;
import javax.swing.WindowConstants;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.pdmodel.PDDocument;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class WorkingDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    
    private JProgressBar prgWorking;
    private JLabel lblWorking;
    private JPanel panel;

    public WorkingDialog(Config config, List<File> pdfFilesIn, List<File> pdfFilesOut, JFrame frmMakenote) {
        setUndecorated(true);
        setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
        addWindowListener(new ThisWindowListener());
        setModal(true);
        setModalityType(ModalityType.APPLICATION_MODAL);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("MakeNote");
        setResizable(false);
        setBounds(100, 100, 315, 60);
        getContentPane().setLayout(null);

        lblWorking = new JLabel("Working...");
        lblWorking.setFont(new Font("Arial", Font.PLAIN, 12));
        lblWorking.setBounds(10, 5, 60, 14);
        getContentPane().add(lblWorking);

        prgWorking = new JProgressBar();
        prgWorking.setBounds(10, 25, 295, 25);
        getContentPane().add(prgWorking);
        
        panel = new JPanel();
        panel.setBorder(new LineBorder(new Color(0, 0, 0)));
        panel.setBounds(0, 0, 315, 60);
        getContentPane().add(panel);
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        int dialogX = frmMakenote.getX() + (frmMakenote.getWidth() - getWidth()) / 2;
        int dialogY = frmMakenote.getY() + (frmMakenote.getHeight() - getHeight()) / 2;
        setLocation(dialogX, dialogY);
        
        SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
            @Override
            protected Void doInBackground() throws Exception {
                // Generate output documents.
                NoteTemplate noteTemplate = new NoteTemplate(config);
                PDDocument inDoc = null;
                PDDocument outDoc = null;
                try {
                    for (int i = 0; i < pdfFilesIn.size(); i++) {
                        inDoc = PDDocument.load(pdfFilesIn.get(i));
                        outDoc = noteTemplate.generateNoteFromTemplate(inDoc);
                        outDoc.save(pdfFilesOut.get(i));

                        inDoc.close();
                        outDoc.close();
                        
                        int progress = (int) (100 * (i + 1) / (double) pdfFilesIn.size());
                        prgWorking.setValue(progress);
                        
                        if (progress == 100) {
                            Thread.sleep(500);
                            dispose();
                        }
                    }
                } catch (IOException ioe) {
                    JOptionPane.showMessageDialog(frmMakenote, "Error generating output document.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                } catch (COSVisitorException e) {
                    JOptionPane.showMessageDialog(frmMakenote, "Could not save output document.", "MakeNote - Error", JOptionPane.ERROR_MESSAGE);
                }
                
                return null;
            }
        };
        
        worker.execute();
    }
    
    private class ThisWindowListener extends WindowAdapter {
        @Override
        public void windowClosing(WindowEvent arg0) {
            return;
        }
        @Override
        public void windowClosed(WindowEvent e) {
            return;
        }
    }
}
