/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;

class NoteSegment extends Segment {
    private PDPage page;

    public NoteSegment(int width, int height, int xpos, int ypos, Config config) {
        super(width, height, xpos, ypos, config);

        page = new PDPage(new PDRectangle(width, height));
    }

    @Override
    public BufferedImage render() throws IOException {
        // Create blank segment.
        BufferedImage segmentImg = new BufferedImage(getWidth(), getHeight(), BufferedImage.TYPE_INT_RGB);

        // Allocate graphics stream and draw white background.
        Graphics2D g2D = segmentImg.createGraphics();
        g2D.setColor(getBgColor());
        g2D.fillRect(0, 0, getWidth(), getHeight());

        // Convert PDF page to image.
        BufferedImage pageImg = page.convertToImage(BufferedImage.TYPE_INT_RGB, getDpi());

        // Determine scale factor and scale image to fit segment.
        double scaleFactor = Math.min((double) getWidth() / pageImg.getWidth(), (double) getHeight() / pageImg.getHeight());
        BufferedImage scaledPageImg = Utils.scaleImage(pageImg, scaleFactor);

        // Center the scaledPageImg onto the segment.
        int centerX = (segmentImg.getWidth() - scaledPageImg.getWidth()) / 2;
        int centerY = (segmentImg.getHeight() - scaledPageImg.getHeight()) / 2;
        g2D.drawImage(scaledPageImg, centerX, centerY, null);

        // If outline is enabled draw it.
        if (isOutline()) {
            g2D.setColor(getFgColor());
            g2D.drawRect(0, 0, getWidth(), getHeight());
        }

        // Free resources.
        g2D.dispose();
        pageImg.flush();

        // Return rendered segment.
        return segmentImg;
    }

    public void setPage(PDPage page) {
        if (page == null) {
            return;
        }
        this.page.clear();
        this.page = page;
    }
}