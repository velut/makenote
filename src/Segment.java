/*
 * Copyright 2015 Alam I.
 * 
 * This file is part of MakeNote.
 *
 * MakeNote is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License.
 * 
 * MakeNote is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with MakeNote. If not, see <http://www.gnu.org/licenses/>.
 */

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;

abstract class Segment {
    private int width;
    private int height;
    private int xpos;
    private int ypos;
    
    private int dpi;
    
    private Color bgColor;
    private Color fgColor;
    
    private int spacing;
    
    private boolean outline;
    
    protected Segment(int width, int height, int xpos, int ypos, Config config) {
        this.width = width;
        this.height = height;
        this.xpos = xpos;
        this.ypos = ypos;
        
        dpi = config.getDpi();
        
        bgColor = Color.WHITE;
        fgColor = Color.BLACK;
        
        spacing = config.getSpacingPx();
        
        outline = config.isOutlineEnabled();
    }
    
    abstract public BufferedImage render() throws IOException;

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getXpos() {
        return xpos;
    }

    public int getYpos() {
        return ypos;
    }
    
    public int getDpi() {
        return dpi;
    }
    
    public Color getBgColor() {
        return bgColor;
    }

    public Color getFgColor() {
        return fgColor;
    }

    public int getSpacing() {
        return spacing;
    }

    public boolean isOutline() {
        return outline;
    }
}